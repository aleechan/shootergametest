#include "Box.h"



void Box::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
}

Box::Box(sf::Vector2f pos, sf::Vector2f size, sf::Color c)
{
	this->rect = sf::FloatRect(pos, size);
	this->c = c;
}

sf::FloatRect Box::getRect()
{
	return this->rect;
}

Box::~Box()
{
}
