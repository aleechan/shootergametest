#include "Hull.h"



void Hull::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	sf::VertexArray verts(sf::TrianglesFan,2+lines.size());
	verts[0].position = center;
	verts[0].color = sf::Color::Transparent;
	for (int i = 0; i < lines.size(); i++)
	{
		verts[i+1].position = lines[i].P0();
		verts[i+1].color = sf::Color::Blue;
	}
	verts[lines.size()+1].position = lines[0].P0();
	verts[lines.size()+1].color = sf::Color::Blue;
	target.draw(verts, states);
}

Hull::Hull(sf::Vector2f * points, int count)
{
	if (count > 1)
	{
		center = points[0];
		for (int i = 1; i < count; i++)
		{
			center += points[i];
			lines.push_back(Line(points[i - 1], points[i]));
		}
		lines.push_back(Line(points[count-1], points[0]));
		center /= (float)count;
	}
}

void Hull::rotate(float deg)
{
	rotate(deg, this->center);
}

void Hull::rotate(float deg, sf::Vector2f center)
{
	sf::Transform trans;
	trans.rotate(deg, center);
	this->center = trans.transformPoint(this->center);
	for (int i = 0; i < lines.size(); i++)
	{
		lines[i].setP0(trans.transformPoint(lines[i].P0()));
		lines[i].setP1(trans.transformPoint(lines[i].P1()));
	}
}

void Hull::translate(sf::Vector2f d)
{
	sf::Transform trans;
	trans.translate(d);
	center = trans.transformPoint(center);
	for (int i = 0; i < lines.size(); i++)
	{
		lines[i].setP0(trans.transformPoint(lines[i].P0()));
		lines[i].setP1(trans.transformPoint(lines[i].P1()));
	}
}

bool Hull::clipLine(Line &l)
{
	bool clipped = false;
	sf::Vector2f temp = sf::Vector2f(0, 0);
	for (int i = 0; i < this->lines.size(); i++)
	{
		if (getIntersection(l, lines[i], temp))
		{
			l.setP1(temp);
			clipped = true;
		}
	}
	return clipped;
}

bool Hull::clipLine(Line & l, L2HManifold &m)
{
	bool clipped = false;
	sf::Vector2f temp = sf::Vector2f(0, 0);
	for (int i = 0; i < this->lines.size(); i++)
	{
		if (getIntersection(l, lines[i], temp))
		{
			l.setP1(temp);
			m.L0 = &l;
			m.L1 = &lines[i];
			m.point = temp;
			m.normal = lines[i].lNormal();
			clipped = true;
		}
	}
	return clipped;
}

Hull::~Hull()
{
}
