#include "Line.h"

Line::Line(sf::Vector2f p0, sf::Vector2f p1)
{
	this->p0 = p0;
	this->p1 = p1;
}

sf::Vector2f Line::P0() const
{
	return p0;
}

sf::Vector2f Line::P1() const
{
	return p1;
}

sf::Vector2f Line::getVect()
{
	sf::Vector2f temp = p1 - p0;
	temp /= sqrt(temp.x*temp.x + temp.y*temp.y);
	return temp;
}

sf::Vector2f Line::rNormal()
{
	sf::Vector2f temp = this->getVect();
	return sf::Vector2f(-temp.y,temp.x);
}

sf::Vector2f Line::lNormal()
{
	sf::Vector2f temp = this->getVect();
	return sf::Vector2f(temp.y,-temp.x);
}

void Line::setP0(sf::Vector2f p)
{
	this->p0 = p;
}

void Line::setP1(sf::Vector2f p)
{
	this->p1 = p;
}

Line::~Line()
{
}

bool getIntersection(Line l0, Line l1, sf::Vector2f & point)
{
	float d = (l0.P0().x-l0.P1().x)*(l1.P0().y - l1.P1().y) - (l0.P0().y - l0.P1().y)*(l1.P0().x - l1.P1().x);
	if (d == 0.f)// are the lines parallel
	{
		return false;
	}
	else//find the intesection as if they were lines (not segments)
	{
		point.x = (l0.P0().x*l0.P1().y - l0.P0().y*l0.P1().x) * (l1.P0().x - l1.P1().x) - (l0.P0().x - l0.P1().x) * (l1.P0().x*l1.P1().y - l1.P0().y*l1.P1().x); 
		point.y = (l0.P0().x*l0.P1().y - l0.P0().y*l0.P1().x) * (l1.P0().y - l1.P1().y) - (l0.P0().y - l0.P1().y) * (l1.P0().x*l1.P1().y - l1.P0().y*l1.P1().x);
		point /= d;

		sf::Vector2f max, min;

		//check if the point is on l0
		if (l0.P0().x < l0.P1().x)
		{
			min.x = l0.P0().x;
			max.x = l0.P1().x;
		}
		else
		{
			min.x = l0.P1().x;
			max.x = l0.P0().x;
		}

		if (l0.P0().y < l0.P1().y)
		{
			min.y = l0.P0().y;
			max.y = l0.P1().y;
		}
		else
		{
			min.y = l0.P1().y;
			max.y = l0.P0().y;
		}

		if (min.x > point.x || point.x > max.x)
		{
			return false;
		}
		if (min.y > point.y || point.y > max.y)
		{
			return false;
		}

		//check if the point is on l1
		if (l1.P0().x < l1.P1().x)
		{
			min.x = l1.P0().x;
			max.x = l1.P1().x;
		}
		else
		{
			min.x = l1.P1().x;
			max.x = l1.P0().x;
		}

		if (l1.P0().y < l1.P1().y)
		{
			min.y = l1.P0().y;
			max.y = l1.P1().y;
		}
		else
		{
			min.y = l1.P1().y;
			max.y = l1.P0().y;
		}

		if (min.x > point.x || point.x > max.x)
		{
			return false;
		}
		if (min.y > point.y || point.y > max.y)
		{
			return false;
		}

		return true;
	}
}
