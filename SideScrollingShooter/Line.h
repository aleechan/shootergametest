#ifndef LINE_H
#define LINE_H
#include <SFML\System.hpp>
#include <SFML\Graphics\Rect.hpp>
class Line //a line segment
{
	sf::Vector2f p0, p1;
public:
	Line(sf::Vector2f p0, sf::Vector2f p1);
	sf::Vector2f P0() const;
	sf::Vector2f P1() const;
	sf::Vector2f getVect();
	sf::Vector2f rNormal();
	sf::Vector2f lNormal();
	void setP0(sf::Vector2f p);
	void setP1(sf::Vector2f p);
	~Line();
};

bool getIntersection(Line l0, Line l1, sf::Vector2f &point);
#endif
