#ifndef BOX_H
#define BOX_H
#include <SFML\Main.hpp>
#include <SFML\Graphics.hpp>
class Box : public sf::Drawable
{
private:
	sf::FloatRect rect;
	sf::Color c;
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
public:
	Box(sf::Vector2f pos, sf::Vector2f size,sf::Color c);
	sf::FloatRect getRect();
	~Box();
};

#endif