#ifndef HULL_H
#define HULL_H
#include "Line.h"
#include <SFML\Graphics.hpp>

struct L2HManifold
{
	Line *L0, *L1;
	sf::Vector2f normal;
	sf::Vector2f point;
};

class Hull : public sf::Drawable// set of line, and points that make a closed shape
{
	std::vector<Line> lines;
	sf::Vector2f center;//the average of all the points
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

public:
	Hull(sf::Vector2f *points, int count);//list points in clockwise order
	void rotate(float deg);// rotate about the center of the hull
	void rotate(float deg, sf::Vector2f center);//rotate about the give point
	void translate(sf::Vector2f d);//move the hull relative to the current location
	bool clipLine(Line &l);
	bool clipLine(Line &l, L2HManifold &m);
	virtual ~Hull();
};
#endif

