#include <SFML/Graphics.hpp>
#include "Hull.h"

int main()
{
	sf::ContextSettings settings;
	settings.antialiasingLevel = 4;

	sf::RenderWindow window(sf::VideoMode(1280, 720), "Shooter Test", sf::Style::Default, settings);
	window.setFramerateLimit(60);

	sf::Vector2f points[5];
	points[0] = sf::Vector2f(100, 100);
	points[1] = sf::Vector2f(150, 50);
	points[2] = sf::Vector2f(300, 100);
	points[3] = sf::Vector2f(200, 200);
	points[4] = sf::Vector2f(100, 200);

	Hull test(points, 5);

	points[0] = sf::Vector2f(200, 200);
	points[1] = sf::Vector2f(250, 250);
	points[2] = sf::Vector2f(400, 200);
	points[3] = sf::Vector2f(300, 300);
	points[4] = sf::Vector2f(200, 300);

	Hull test2(points, 5);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		sf::VertexArray verts(sf::Lines, 480);
		int counter = 240;
		for (int i = 0; i < 120; i++)
		{
			L2HManifold manifold;
			Line l(sf::Vector2f(0, 110 + 5*i), sf::Vector2f(1000, 112 + 10 * i));
			if (test.clipLine(l, manifold) || test2.clipLine(l, manifold))
			{
				verts[counter].position = manifold.point;
				sf::Vector2f lVect = l.getVect();
				sf::Vector2f temp = l.P1() - l.P0();
				float remaining = 1000.f - sqrt(temp.x*temp.x + temp.y*temp.y);
				verts[counter + 1].position = manifold.point + (lVect- 2.f*(lVect.x*manifold.normal.x + lVect.y*manifold.normal.y)*manifold.normal)*remaining;
				counter += 2;
			}
			verts[2 * i].position = l.P0();
			verts[2 * i + 1].position = l.P1();
		}


		window.clear();
		window.draw(test);
		window.draw(test2);
		window.draw(verts);
		test.rotate(1,sf::Vector2f(300,300));
		test2.rotate(0.5, sf::Vector2f(450, 425));
		//test.rotate(2);
		window.display();
	}

	return 0;
}