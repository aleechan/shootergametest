//#include "my_math.h"
#include <SFML\Window.hpp>

#ifndef _LINES_SYS
#define _LINES_SYS

class line
{
public:
	sf::Vector2f p1, p2;
	void init(sf::Vector2f start, sf::Vector2f end)
	{
		p1 = start;
		p2 = end;
	}
};

bool line_collide(line l1, line l2, sf::Vector2f &col)
{
	sf::Vector2f p1,p2,p3,p4;
	if (l1.p1.x < l1.p2.x)
	{
		p1 = l1.p1;
		p2 = l1.p2;
	}
	else
	{
		p1 = l1.p2;
		p2 = l1.p1;
	}
	if (l2.p1.x < l2.p2.x)
	{
		p3 = l2.p1;
		p4 = l2.p2;
	}
	else
	{
		p3 = l2.p2;
		p4 = l2.p1;
	}

	double d = (p1.x-p2.x)*(p3.y-p4.y)-(p1.y-p2.y)*(p3.x-p4.x);
	if (d!=0)
	{
		col.x  = ( (p1.x*p2.y-p1.y*p2.x)*(p3.x-p4.x)-(p1.x-p2.x)*(p3.x*p4.y-p3.y*p4.x) )/d;
		col.y  = ( (p1.x*p2.y-p1.y*p2.x)*(p3.y-p4.y)-(p1.y-p2.y)*(p3.x*p4.y-p3.y*p4.x) )/d;
		float x1,x2,y1,y2;
		x1 = p1.x;
		x2 = p2.x;
		if(p1.y > p2.y)
		{
			y1 = p2.y;
			y2 = p1.y;
		}
		else
		{
			y1 = p1.y;
			y2 = p2.y;
		}
		if ( col.x>=x1-1 && col.x<=x2+1 && col.y>=y1-1 && col.y<=y2+1 )
		{
			x1 = p3.x;
			x2 = p4.x;
			if(p3.y > p4.y)
			{
				y1 = p4.y;
				y2 = p3.y;
			}
			else
			{
				y1 = p3.y;
				y2 = p4.y;
			}
			if ( col.x>=x1-1 && col.x<=x2+1 && col.y>=y1-1 && col.y<=y2+1 )
			{
				return true;
			}
		}
	}
	col = l1.p2;
	return false;
}

line normal_rel(line l)
{
	line n;
	n.p1.x = l.p1.x + (l.p2.x - l.p1.x)/2;
	n.p1.y = l.p1.y + (l.p2.y - l.p1.y)/2;
	n.p2 = n.p1 + sf::Vector2f( l.p2.y - l.p1.y , l.p1.x - l.p2.x );
	return n;
}

line normal(line l)
{
	line n;
	n.p1 = sf::Vector2f(0,0);
	n.p2 = sf::Vector2f( l.p2.y - l.p1.y , l.p1.x - l.p2.x );
	return n;
}

#endif