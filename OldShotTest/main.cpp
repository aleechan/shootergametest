#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML\System\Thread.hpp>
#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <math.h>
#include <zmouse.h>
#include <sstream>
#include <time.h>
#include "lines.h"

const double degtorad = 0.0174532925;
const double radtodeg = 57.2957795;
const double PI = 3.141592654;

const int width = 800;
const int height = 600;

#include "shaders.h"

const int partCap = 200000;
const int emitCap = 1000;
const double G = 10000;

static bool calc = true;

int mousex = sf::Mouse::getPosition().x;
int mousey = sf::Mouse::getPosition().y;

int irandom(int v)
{
	return (int)( (v*rand()/RAND_MAX) +0.5);
}

double point_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (sqrt(a));
}

double sqr_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (a);
}

double point_direction(double x1, double y1, double x2, double y2)
{
	double cx = x2-x1;
	double cy = y2-y1;
	double a = atan2(cy,cx);
	return a;
}

sf::Vector2f trig[360];
float icos(int i)
{
	while(i>360)
	{
		i-=360;
	}
	while(i<0)
	{
		i+=360;
	}
	return(trig[i].x);
}
float isin(int i)
{
	while(i>360)
	{
		i-=360;
	}
	while(i<0)
	{
		i+=360;
	}
	return(trig[i].y);
}

float dot(sf::Vector2f v1, sf::Vector2f v2)
{
	return(v1.x*v2.x+v1.y*v2.y);
}

class wall
{
public:
	double x,y,width, height;
	sf::Vector2f points[4];
	sf::RectangleShape self;
	void init(double x1,double y1,double width2,double height2)
	{
		x = x1;
		y = y1;
		width = width2;
		height = height2;
		self.setPosition(x,y);
		self.setSize(sf::Vector2f(width,height));
		self.setOrigin(width/2,height/2);
		self.setFillColor(sf::Color::Blue);
		self.setRotation(45);
		points[0] = sf::Vector2f(x-width/2,y);
		points[1] = sf::Vector2f(x,y-height/2);
		points[2] = sf::Vector2f(x+width/2,y);
		points[3] = sf::Vector2f(x,y+height/2);
		//points[0] = sf::Vector2f(x-width/2,y-height/2);
		//points[1] = sf::Vector2f(x+width/2,y-height/2);
		//points[2] = sf::Vector2f(x+width/2,y+height/2);
		//points[3] = sf::Vector2f(x-width/2,y+height/2);
	}
};

void fire_shot(double x, double y, double tx, double ty, int spread, int flashPts, int flashsize,  int impactPts, int impactsize, sf::RenderWindow &app, wall *blocks)
{
	sf::Vector2f colpoint, temp;
	sf::CircleShape dot;
	
	dot.setFillColor(sf::Color::Red);
	dot.setRadius(5);
	dot.setOrigin(5,5);

	//float x1,x2,x3,x4;
	//float y1,y2,y3,y4;
	float d,temp0;
	
	bool impact = false;

	line tempL, shot;
	shot.p1 = sf::Vector2f(x,y);

	colpoint = sf::Vector2f(tx,ty);

	shot.p2 = colpoint;
	for (int i  = 0 ; i < 10; i++)
	{
		tempL.init(blocks[i].points[0],blocks[i].points[1]);
		if(line_collide(shot,tempL,temp))
		{
			if(sqr_distance(x,y,temp.x,temp.y) < sqr_distance(x,y,colpoint.x,colpoint.y))
			{
				colpoint = temp;
				shot.p2 = colpoint;
				impact = true;
			}
		}

		tempL.init(blocks[i].points[1],blocks[i].points[2]);
		if(line_collide(shot,tempL,temp))
		{
			if(sqr_distance(x,y,temp.x,temp.y) < sqr_distance(x,y,colpoint.x,colpoint.y))
			{
				colpoint = temp;
				shot.p2 = colpoint;
				impact = true;
			}
		}
		tempL.init(blocks[i].points[2],blocks[i].points[3]);
		if(line_collide(shot,tempL,temp))
		{
			if(sqr_distance(x,y,temp.x,temp.y) < sqr_distance(x,y,colpoint.x,colpoint.y))
			{
				colpoint = temp;
				shot.p2 = colpoint;
				impact = true;
			}
		}
		tempL.init(blocks[i].points[3],blocks[i].points[0]);
		if(line_collide(shot,tempL,temp))
		{
			if(sqr_distance(x,y,temp.x,temp.y) < sqr_distance(x,y,colpoint.x,colpoint.y))
			{
				colpoint = temp;
				shot.p2 = colpoint;
				impact = true;
			}
		}
	}

	dot.setPosition(colpoint);
	sf::VertexArray flash;
	flash.setPrimitiveType(sf::Quads);
	if(impact){flash.resize(4+flashPts*4+impactPts*4);}
	else{flash.resize(2+flashPts*4);}
	int count = 0;

	line n;
	n.init(shot.p1,shot.p2);
	n = normal(n);
	n.p2.x /= point_distance(n.p1.x,n.p1.y,n.p2.x,n.p2.y);
	n.p2.y /= point_distance(n.p1.x,n.p1.y,n.p2.x,n.p2.y);

	flash[count].color = sf::Color::White;
	flash[count].position = shot.p1 + sf::Vector2f(5*n.p2.x,5*n.p2.y);
	count++;

	flash[count].color = sf::Color::White;
	flash[count].position = shot.p2 + sf::Vector2f(5*n.p2.x,5*n.p2.y);
	count++;
		
	flash[count].color = sf::Color::White;
	flash[count].position = shot.p2 - sf::Vector2f(5*n.p2.x,5*n.p2.y);
	count++;

	flash[count].color = sf::Color::White;
	flash[count].position = shot.p1 - sf::Vector2f(5*n.p2.x,5*n.p2.y);
	count++;

	double sinmin,sinmax,cosmin,cosmax,dir,r,dx,dy;
	dir = point_direction(x,y,colpoint.x,colpoint.y) + PI;
	sinmin = sin(dir-15);
	sinmax = sin(dir+15);
	dy = sinmax-sinmin;
	cosmin = cos(dir-15);
	cosmax = cos(dir+15);
	dx = cosmax-cosmin;
	
	double sinN,cosN,c,wid;
	sf::Vector2f t1,t2;
	wid = 2;
	
	for(double i=0; i<flashPts; i++)
	{
		r = 1+0.1*irandom(10*flashsize);
		t1 = shot.p1;
		t2 = sf::Vector2f(x + r*( cosmin+dx*(i/flashPts) ),y + r*( sinmin+dy*(i/flashPts) ));

		cosN = (t2.y-t1.y)/r;
		sinN = -1*(t2.x-t1.x)/r;

		flash[count].color = sf::Color::White;
		flash[count].position = t1+sf::Vector2f(wid*cosN,wid*sinN);
		count++;

		flash[count].color = sf::Color::White;
		flash[count].position = t1-sf::Vector2f(wid*cosN,wid*sinN);
		count++;

		flash[count].color = sf::Color::White;
		flash[count].position = t2+sf::Vector2f(wid*cosN,wid*sinN);
		count++;

		flash[count].color = sf::Color::White;
		flash[count].position = t2-sf::Vector2f(wid*cosN,wid*sinN);
		count++;
	}
	if(impact)
	{
		dir = point_direction(x,y,colpoint.x,colpoint.y);
		sinmin = sin(dir-15);
		sinmax = sin(dir+15);
		dy = sinmax-sinmin;
		cosmin = cos(dir-15);
		cosmax = cos(dir+15);
		dx = cosmax-cosmin;
		for(double i=0; i<impactPts; i++)
		{
			r = 1+0.1*irandom(10*flashsize);
			t1 = shot.p2;
			t2 = sf::Vector2f(shot.p2.x + r*( cosmin+dx*(i/impactPts) ),shot.p2.y + r*( sinmin+dy*(i/impactPts) ));

			cosN = (t2.y-t1.y)/r;
			sinN = -1*(t2.x-t1.x)/r;

			flash[count].color = sf::Color::White;
			flash[count].position = t1+sf::Vector2f(wid*cosN,wid*sinN);
			count++;

			flash[count].color = sf::Color::White;
			flash[count].position = t1-sf::Vector2f(wid*cosN,wid*sinN);
			count++;

			flash[count].color = sf::Color::White;
			flash[count].position = t2+sf::Vector2f(wid*cosN,wid*sinN);
			count++;

			flash[count].color = sf::Color::White;
			flash[count].position = t2-sf::Vector2f(wid*cosN,wid*sinN);
			count++;
		}
	}
	app.draw(flash);
	//app.draw(dot);
}

void fire_shot(double x, double y, double tx, double ty, int spread, int flashPts, int flashsize,  int impactPts, int impactsize, sf::RenderTexture &app, wall *blocks)
{
	sf::Vector2f colpoint, temp;
	sf::CircleShape dot;
	
	dot.setFillColor(sf::Color::Red);
	dot.setRadius(5);
	dot.setOrigin(5,5);

	//float x1,x2,x3,x4;
	//float y1,y2,y3,y4;
	float d,temp0;
	
	bool impact = false;

	line tempL, shot;
	shot.p1 = sf::Vector2f(x,y);

	colpoint = sf::Vector2f(tx,ty);

	shot.p2 = colpoint;
	for (int i  = 0 ; i < 10; i++)
	{
		tempL.init(blocks[i].points[0],blocks[i].points[1]);
		if(line_collide(shot,tempL,temp))
		{
			if(sqr_distance(x,y,temp.x,temp.y) < sqr_distance(x,y,colpoint.x,colpoint.y))
			{
				colpoint = temp;
				shot.p2 = colpoint;
				impact = true;
			}
		}

		tempL.init(blocks[i].points[1],blocks[i].points[2]);
		if(line_collide(shot,tempL,temp))
		{
			if(sqr_distance(x,y,temp.x,temp.y) < sqr_distance(x,y,colpoint.x,colpoint.y))
			{
				colpoint = temp;
				shot.p2 = colpoint;
				impact = true;
			}
		}
		tempL.init(blocks[i].points[2],blocks[i].points[3]);
		if(line_collide(shot,tempL,temp))
		{
			if(sqr_distance(x,y,temp.x,temp.y) < sqr_distance(x,y,colpoint.x,colpoint.y))
			{
				colpoint = temp;
				shot.p2 = colpoint;
				impact = true;
			}
		}
		tempL.init(blocks[i].points[3],blocks[i].points[0]);
		if(line_collide(shot,tempL,temp))
		{
			if(sqr_distance(x,y,temp.x,temp.y) < sqr_distance(x,y,colpoint.x,colpoint.y))
			{
				colpoint = temp;
				shot.p2 = colpoint;
				impact = true;
			}
		}
	}


	dot.setPosition(colpoint);

	sf::VertexArray flash,linee,impactF;

	flash.setPrimitiveType(sf::TrianglesFan);
	linee.setPrimitiveType(sf::Quads);
	impactF.setPrimitiveType(sf::TrianglesFan);

	linee.resize(4);
	flash.resize(1+flashPts);
	int count = 0;

	line n;
	n.init(shot.p1,shot.p2);
	n = normal(n);
	n.p2.x /= point_distance(n.p1.x,n.p1.y,n.p2.x,n.p2.y);
	n.p2.y /= point_distance(n.p1.x,n.p1.y,n.p2.x,n.p2.y);

	linee[count].color = sf::Color::White;
	linee[count].position = shot.p1 + sf::Vector2f(3*n.p2.x,3*n.p2.y);
	count++;

	linee[count].color = sf::Color::White;
	linee[count].position = shot.p2 + sf::Vector2f(3*n.p2.x,3*n.p2.y);
	count++;
		
	linee[count].color = sf::Color::White;
	linee[count].position = shot.p2 - sf::Vector2f(3*n.p2.x,3*n.p2.y);
	count++;

	linee[count].color = sf::Color::White;
	linee[count].position = shot.p1 - sf::Vector2f(3*n.p2.x,3*n.p2.y);
	count++;

	double sinmin,sinmax,cosmin,cosmax,dir,r,dx,dy;
	dir = point_direction(x,y,colpoint.x,colpoint.y) + PI;
	sinmin = sin(dir-15);
	sinmax = sin(dir+15);
	dy = sinmax-sinmin;
	cosmin = cos(dir-15);
	cosmax = cos(dir+15);
	dx = cosmax-cosmin;
	
	count = 0;
	flash[count].color = sf::Color::White;
	flash[count].position = shot.p1;
	count++;
	for(double i=0; i<flashPts; i++)
	{
		r = 3*flashsize/4+0.1*irandom((int)(10*flashsize/2))-flashsize/4;

		flash[count].color = sf::Color::Cyan;
		flash[count].position = sf::Vector2f(x + r*( cosmin+dx*(i/flashPts) ),y + r*( sinmin+dy*(i/flashPts) ));
		count++;
	}
	if(impact)
	{
		count = 0;
		impactF.resize(1+impactPts);
		dir = point_direction(x,y,colpoint.x,colpoint.y);
		sinmin = sin(dir-15);
		sinmax = sin(dir+15);
		dy = sinmax-sinmin;
		cosmin = cos(dir-15);
		cosmax = cos(dir+15);
		dx = cosmax-cosmin;
		impactF[count].color = sf::Color::White;
		impactF[count].position = shot.p2;
		count++;
		for(double i=0; i<impactPts; i++)
		{

			r = impactsize/2+0.1*irandom((int)(10*impactsize/2));

			impactF[count].color = sf::Color::White;
			impactF[count].position = sf::Vector2f(shot.p2.x + r*( cosmin+dx*(i/impactPts) ),shot.p2.y + r*( sinmin+dy*(i/impactPts) ));
			count++;
		}
	}
	app.draw(linee);
	app.draw(flash);
	app.draw(impactF);
	//app.draw(dot);
}


int main()
{
	std::cout << sf::Texture::getMaximumSize();
    sf::RenderWindow App(sf::VideoMode (width, height, 32), "turret");
	
	App.setFramerateLimit(60);
	//App.setVerticalSyncEnabled(true);

	for(int i = 0; i<=360;i++)
	{
		trig[i] = sf::Vector2f( cos(i*degtorad),sin(i*degtorad) );
	}

	gaus_blur blur;
	blur.init();

	int x = 25;
	int y = (int)height/2;

	mousex = sf::Mouse::getPosition(App).x;
	mousey = sf::Mouse::getPosition(App).y;

	sf::Texture srifle;
	srifle.loadFromFile("sprsrifle.png");

	sf::Sprite gun;
	gun.setTexture(srifle);
	gun.setOrigin(25,10);
	gun.setPosition(x,y);

	int counts = 0;
	int dir;

	wall blocks[10];
	for(int i = 0; i<10; i++)
	{
		blocks[i].init(450-50+irandom(100),64*i,32,32);
	}


	sf::Clock clock;
    float lastTime = 0;

    // Start game loop
    while (App.isOpen())
     {
         // Process events
         sf::Event event;
         while (App.pollEvent(event))
         {
             // Close window : exit
             if (event.type == sf::Event::Closed)
                 App.close();
         }
		 App.clear();
		 blur.clear();

			mousex = sf::Mouse::getPosition(App).x;
			mousey = sf::Mouse::getPosition(App).y;
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				y--;
				gun.setPosition(x,y);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			{
				y++;
				gun.setPosition(x,y);
			}

			dir = radtodeg*point_direction(x,y,mousex,mousey);
			gun.setRotation(dir);

			App.draw(gun);
			sf::VertexArray normals(sf::Lines,80);
			line tempL,tempN;
			int count = 0;
			for(int i = 0; i<10; i++)
			{
				tempL.init(blocks[i].points[0],blocks[i].points[1]);
				tempN = normal_rel(tempL);

				normals[count].color = sf::Color::Red;
				normals[count].position = tempN.p1;
				count++;

				normals[count].color = sf::Color::White;
				normals[count].position = tempN.p2;
				count++;

				tempL.init(blocks[i].points[1],blocks[i].points[2]);
				tempN = normal_rel(tempL);

				normals[count].color = sf::Color::Red;
				normals[count].position = tempN.p1;
				count++;

				normals[count].color = sf::Color::White;
				normals[count].position = tempN.p2;
				count++;

				tempL.init(blocks[i].points[2],blocks[i].points[3]);
				tempN = normal_rel(tempL);

				normals[count].color = sf::Color::Red;
				normals[count].position = tempN.p1;
				count++;

				normals[count].color = sf::Color::White;
				normals[count].position = tempN.p2;
				count++;

				tempL.init(blocks[i].points[3],blocks[i].points[0]);
				tempN = normal_rel(tempL);

				normals[count].color = sf::Color::Red;
				normals[count].position = tempN.p1;
				count++;

				normals[count].color = sf::Color::White;
				normals[count].position = tempN.p2;
				count++;

				App.draw(blocks[i].self);
			}
			if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				if (counts > 5)
				{
					//fire_shot(x+92*cos(degtorad*dir),y+92*sin(degtorad*dir),x+(width+height)*cos(degtorad*dir),y+(width+height)*sin(degtorad*dir),0,14,30,14,15,App,blocks);
					fire_shot(x+92*cos(degtorad*dir),y+92*sin(degtorad*dir),x+(width+height)*cos(degtorad*dir),y+(width+height)*sin(degtorad*dir),0,14,75,14,45,blur.original,blocks);
					counts=0;
				}
			}
			counts++;

			App.draw(normals);
			App.draw(blur.render());

			float currentTime = clock.restart().asSeconds();
			float fps = 1.f / currentTime;
			lastTime = currentTime;
			std::stringstream out;
			out << "FPS: " << (int)fps;
			App.setTitle(out.str());

			App.display();

    }

    return EXIT_SUCCESS;
}
